<?php
if (isset($message)) {
    echo $message;
} else if ($this->session->flashdata('message') != '') {
    echo $this->session->flashdata('message');

}
if (isset($_GET)) {
    $title1 = '';
    if (isset($_GET['key']) && $_GET['key'] <> '') {
        $title1 .= " Searching Key :<strong> " . $_GET['key'] . '</strong> &nbsp; &nbsp; &nbsp;';
    }

    if (isset($_GET['from']) && $_GET['from'] <> '') {
        $title1 .= " From :<strong> " .  $_GET['from'] . '</strong> &nbsp; &nbsp; &nbsp;';
    }

    if (isset($_GET['to']) && $_GET['to'] <> '') {
        $title1 .= " Until :<strong> " . $_GET['to'] . '</strong>';
    }

    if ($title1 <> '') {
        echo '<div class="alert alert-warning">' . $title1 . '</div>';
    }

}

?>

<div class="ibox">
    <div class="ibox-title clearfix">
        <h5>Application Collection Fee</h5>
        <span class="pull-right" style="font-weight: bold; color: brown;">
            Amount Collected : <?php echo number_format($this->db->query("SELECT SUM(amount) as amount FROM application_payment")->row()->amount,2); ?>
        </span>
    </div>
    <div class="ibox-content">
        <?php echo form_open(site_url('collection'), ' method="GET" class="form-horizontal ng-pristine ng-valid"') ?>
        <div class="form-group no-padding">
            <div class="col-md-3 col-md-offset-1" style="padding-left: 0px;">
                <input type="text" value="<?php echo(isset($_GET['key']) ? $_GET['key'] : '') ?>" name="key"
                       class="form-control" placeholder="Search....">
            </div>
            <div class="col-md-3">
                <input name="from" placeholder="FROM : DD-MM-YYYY" class="form-control mydate_input" value="<?php echo(isset($_GET['from']) ? $_GET['from'] : '') ?>"/>

            </div>
            <div class="col-md-3">
                <input name="to" placeholder="FROM : DD-MM-YYYY" class="form-control mydate_input" value="<?php echo(isset($_GET['to']) ? $_GET['to'] : '') ?>"/>            </div>
            <div class="col-md-1">
                <input type="submit" value="Search" class="btn btn-success btn-sm">
            </div>
        </div>
        <?php echo form_close();
        ?>
        <div class="table-responsive">
            <table cellspacing="0" cellpadding="0" class="table table-bordered"
                   style="" id="applicantlist">
                <thead>
                <tr>
                    <th style="width: 30px; text-align: center">S/No</th>
                    <th style="width: 200px;">Name</th>
                    <th style="width: 150px; text-align: center;">Time</th>
                    <th style="width: 100px; text-align: center;">Receipt</th>
                    <th style="width: 100px; text-align: center;">Mobile Used</th>
                    <th style="width: 100px; text-align: center;">Amount</th>
                    <th style="width: 100px; text-align: center;">Charge</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $page = ($this->uri->segment(2) ? ($this->uri->segment(2)+1):1 );
                foreach ($collection_list as $key => $value) {
                    ?>
                    <tr>
                        <td style="text-align: right;"><?php  echo $page++; ?></td>
                        <td style="text-align: left;"><?php  echo $value->FirstName.' '.$value->MiddleName.' '.$value->LastName; ?></td>
                        <td style="text-align: center;"><?php  echo $value->createdon; ?></td>
                        <td  style="text-align: center;"><?php echo $value->receipt; ?></td>
                        <td  style="text-align: center;"><?php echo $value->msisdn; ?></td>
                        <td style="text-align: right;"><?php echo number_format($value->amount); ?></td>
                        <td style="text-align: right;"><?php echo number_format($value->charges,2); ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <div><?php echo $pagination_links; ?>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("body").on("click",".popup_applicant_info",function () {
            var ID = $(this).attr("ID");
            var title = $(this).attr("title");
            $.confirm({
                title:title,
                content:"URL:<?php echo site_url('popup_applicant_info') ?>/"+ID+'/?status=1',
                confirmButton:'Print',
                columnClass:'col-md-10 col-md-offset-2',
                cancelButton:'Close',
                extraButton:'ExtraB',
                cancelButtonClass: 'btn-success',
                confirmButtonClass: 'btn-success',
                confirm:function () {
                    window.location.href = '<?php echo site_url('print_application') ?>/'+ID;
                    return false;
                },
                cancel:function () {
                    return true;
                }

            });
        })
    });
</script>