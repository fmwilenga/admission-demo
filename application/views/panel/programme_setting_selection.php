<link href="<?php echo base_url(); ?>media/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>media/css/style.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>media/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>media/css/select2-bootstrap.css" rel="stylesheet">

<link href="<?php echo base_url(); ?>media/css/plugins/select2/select2.min.css" rel="stylesheet">


<script src="<?php echo base_url(); ?>media/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url(); ?>media/js/plugins/select2/select2.full.min.js"></script>

<script src="<?php echo base_url(); ?>media/js/bootstrap.min.js"></script>
<style>
    body {
        background: #ffffff;
    }
</style>
<?php
if (isset($message)) {
    echo $message;
} else if ($this->session->flashdata('message') != '') {
    echo $this->session->flashdata('message');
}
?>
<div class="ibox-content" style="padding: 0px;margin-right: 5px; border: 0px; font-size: 15px;">
    <div style=" margin-bottom: 20px;"><span
            style="color: darkgreen; font-weight: bold;">SELECTED PROGRAMME :</span> <?php echo $programme_info->Name; ?>
    </div>

        <?php echo form_open(current_full_url(), ' class="form-horizontal ng-pristine ng-valid"');
        ?>
    <div style="margin-left: 10px;">

        <table style="width: 100%; font-size: 12px;">
            <tr>
                <td style="width: 40%;"><label class="control-label">Capacity :<span class="required">*</span> </label></td>
                <td><input type="text"
                           value="<?php echo set_value('capacity', (isset($setting_info) ? $setting_info->capacity : '')); ?>"
                           placeholder="Capacity"
                           class="form-control" name="capacity">
                    <?php echo form_error('capacity'); ?>
                    <br/>
                </td>
            </tr>
            <tr>
                <td style="width: 40%;"><label class="control-label">Direct Applicant % :<span class="required">*</span> </label></td>
                <td><input type="text"
                           value="<?php echo set_value('direct', (isset($setting_info) ? $setting_info->direct : '')); ?>"
                           placeholder="Percentage of the Capacity, remaining will be treated as Equivalent"
                           class="form-control" name="direct">
                    <div style="font-size: 10px; color: blue;">Percentage of the Capacity, remaining will be treated as Equivalent</div>
                    <?php echo form_error('direct'); ?>
                    <br/>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <div style="font-weight: bold; font-size: 15px; margin-bottom: 15px; color: brown; border-bottom: 1px solid brown">
                        Filtering Criteria
                    </div>
                </td>
            </tr>

            <tr>
                <td  colspan="2">

                    <table class="table" style="font-size: 13px;">
                        <thead>
                        <tr>
                            <th style="width: 70%;">Criteria</th>
                            <th>Order</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Highest Point</td>
                            <td><input type="text" class="form-control"></td>
                        </tr>
                        </tbody>
                    </table>

                </td>
            </tr>

        </table>
    </div>
    <div class="form-group" style="margin-top: 20px;">
        <div class="col-lg-6" style="text-align: right;">
            <input class="btn btn-sm btn-success" type="submit" value="Save Information"/>
        </div>
    </div>


        <?php echo form_close(); ?>


</div>

<script>
    $(document).ready(function () {
        $(".select34").select2({
            theme: 'bootstrap',
            placeholder: '[ Select Subject ]',
            allowClear: true
        });


    })

</script>