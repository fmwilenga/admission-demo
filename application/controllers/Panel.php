<?php

/**
 * Created by PhpStorm.
 * User: miltone
 * Date: 5/16/17
 * Time: 10:45 AM
 */
class Panel extends CI_Controller
{
    private $MODULE_ID = '';
    private $GROUP_ID = '';

    function __construct()
    {
        parent::__construct();


        $this->data['CURRENT_USER'] = current_user();

        $this->form_validation->set_error_delimiters('<div class="required">', '</div>');

        $this->data['title'] = 'Administrator';

        $tmp_group = get_user_group();
        $this->GROUP_ID = $this->data['GROUP_ID'] = $tmp_group->id;
        $this->MODULE_ID = $this->data['MODULE_ID'] = $tmp_group->module_id;
    }

    function applicant_list()
    {
        $current_user = current_user();

        if (!has_role($this->MODULE_ID, $this->GROUP_ID, 'APPLICANT', 'applicant_list')) {
            $this->session->set_flashdata("message", show_alert("APPLICANT_LIST :: Access denied !!", 'info'));
            redirect(site_url('dashboard'), 'refresh');
        }

        $this->data['bscrum'][] = array('link' => '#', 'title' => 'Applicant');
        $this->data['bscrum'][] = array('link' => 'applicant_list/', 'title' => 'Applicant List');

        $this->load->library('pagination');

        $where = ' WHERE 1=1 ';

        if (isset($_GET['from']) && $_GET['from'] != '') {
            $frm = $_GET['from'];
            $from = format_date($frm, true);
            $where .= " AND DATE(createdon) >='" . $from . "' ";
        }

        if (isset($_GET['to']) && $_GET['to'] != '') {
            $t = $_GET['to'];
            $to = format_date($t, true);
            $where .= " AND DATE(createdon) <='" . $to . "' ";
        }

        if (isset($_GET['entry']) && $_GET['entry'] != '') {
            $where .= " AND entry_category='" . $_GET['entry'] . "' ";
        }

        if (isset($_GET['status']) && $_GET['status'] != '') {
            $where .= " AND submitted='" . $_GET['status'] . "' ";
        }
        
        if (isset($_GET['type']) && $_GET['type'] != '') {
            $where .= " AND application_type='" . $_GET['type'] . "' ";
        }

        if (isset($_GET['key']) && $_GET['key'] != '') {
            $where .= " AND  (form4_index LIKE '%" . $_GET['key'] . "%' OR  form6_index LIKE '%" . $_GET['key'] . "%' OR FirstName LIKE '%" . $_GET['key'] . "%' OR  LastName LIKE '%" . $_GET['key'] . "%')";
        }


        $sql = " SELECT * FROM application  $where ";
        $sql2 = " SELECT count(id) as counter FROM application  $where ";

        $config["base_url"] = site_url('applicant_list/');

        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        if (count($_GET) > 0)
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");


        $config["total_rows"] = $this->db->query($sql2)->row()->counter;
        $config["per_page"] = 50;
        $config["uri_segment"] = 2;

        include 'include/config_pagination.php';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2) ? $this->uri->segment(2) : 0);
        $this->data['pagination_links'] = $this->pagination->create_links();

        $this->data['applicant_list'] = $this->db->query($sql . " ORDER BY FirstName ASC LIMIT $page," . $config["per_page"])->result();


        $this->data['active_menu'] = 'applicant_list';
        $this->data['content'] = 'panel/applicant_list';
        $this->load->view('template', $this->data);
    }

    function change_status()
    {
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('applicant_id', 'Applicant ID', 'required');

        if ($this->form_validation->run() == true) {
            $array_data = array(
                'status' => $this->input->post('status')
            );

            $applicant_id = $this->input->post('applicant_id');

            $register = $this->applicant_model->update_applicant($array_data, array('id' => $applicant_id));

            echo '<div style="color: #0000cc">Status updated..</div>';
        } else {
            echo $this->input->post('status') . '|' . $this->input->post('applicant_id') . ' The Status field is required';
        }


    }

    function popup_applicant_info($id)
    {
        $id = decode_id($id);
        $APPLICANT = $this->applicant_model->get_applicant($id);
        if ($APPLICANT) {
            $this->data['APPLICANT'] = $APPLICANT;
            $next_kin = $this->applicant_model->get_nextkin_info($APPLICANT->id)->result();
            if (count($next_kin) > 0) {
                $this->data['next_kin'] = $next_kin;
            }

            $referee = $this->applicant_model->get_applicant_referee($APPLICANT->id)->result();
            if (count($referee) > 0) {
                $this->data['academic_referee'] = $referee;
            }

            $sponsor = $this->applicant_model->get_applicant_sponsor($APPLICANT->id)->row();
            if ($sponsor) {
                $this->data['sponsor_info'] = $sponsor;
            }

            $employer = $this->applicant_model->get_applicant_employer($APPLICANT->id)->row();
            if ($employer) {
                $this->data['employer_info'] = $employer;
            }

            $this->data['education_bg'] = $this->applicant_model->get_education_bg(null, $APPLICANT->id);
            $this->data['attachment_list'] = $this->applicant_model->get_attachment($APPLICANT->id);
            $mychoice = $this->applicant_model->get_programme_choice($APPLICANT->id);
            if ($mychoice) {
                $this->data['mycoice'] = $mychoice;
            }
            if (isset($_GET) && isset($_GET['status'])) {
                $this->data['change_status'] = 1;
            }
            $this->load->view('panel/popup_applicant_info', $this->data);
        } else {
            echo show_alert('This request did not pass our security checks.', 'info');
        }

    }

    function manage_criteria($type = 1)
    {
        $current_user = current_user();

        if (!has_role($this->MODULE_ID, $this->GROUP_ID, 'CRITERIA', 'manage_criteria')) {
            $this->session->set_flashdata("message", show_alert("SELECTION_CRITERIA :: Access denied !!", 'info'));
            redirect(site_url('dashboard'), 'refresh');
        }

        $this->data['bscrum'][] = array('link' => '#', 'title' => 'Criteria');
        $this->data['bscrum'][] = array('link' => 'manage_criteria/', 'title' => 'Eligibility Criteria');
        $this->data['selected'] = $type;
        $this->data['programme_list'] = $this->common_model->get_programme(null, $type)->result();
        $this->data['active_menu'] = 'manage_criteria';
        $this->data['content'] = 'panel/manage_criteria';
        $this->load->view('template', $this->data);
    }

    function programme_setting_panel($code = null)
    {
        $current_user = current_user();
        $this->data['CODE'] = $code;
        $ENTRY = (isset($_GET) && isset($_GET['entry']) ? $_GET['entry'] : null);
        if (!is_null($code)) {
            $row_year = $this->common_model->get_academic_year(null, 1, 1)->row();
            if ($row_year) {

                if (isset($_GET['sub_id']) && isset($_GET['cat']) && isset($_GET['row_id'])) {
                    //remove one row in the setting configurations
                    $get_row = $this->db->where('id', $_GET['row_id'])->get('application_criteria_setting')->row();
                    if ($get_row) {
                        $column = 'form6_inclusive';
                        if ($_GET['cat'] == 'IV') {
                            $column = 'form4_inclusive';
                        }

                        $column_data = json_decode($get_row->{$column}, true);
                        unset($column_data[$_GET['sub_id']]);

                        $this->db->update('application_criteria_setting', array($column => json_encode($column_data)), array('id' => $_GET['row_id']));
                        $this->session->set_flashdata('message', show_alert('Setting Data updated successfully', 'info'));
                        redirect(remove_query_string(array('sub_id', 'cat', 'row_id')), 'refresh');

                    }

                }

                //if(isset($_GET) && isset($_GET['type'])) {
                $this->form_validation->set_rules('save_data', 'Save Data', 'required');
                $this->form_validation->set_rules('form4_pass', '', 'integer');

                if ($this->form_validation->run() == true) {

                    $form4_data = array();
                    $form6_data = array();
                    $subject_form4 = $this->input->post('subjectIV');
                    $grade_form4 = $this->input->post('gradeIV');

                    $subject_form6 = $this->input->post('subjectVI');
                    $grade_form6 = $this->input->post('gradeVI');

                    $subjectIVOR = $this->input->post('subjectIVOR');
                    $gradeIVOR = $this->input->post('gradeIVOR');
                    $gradeIVORNO = $this->input->post('gradeIVORNO');

                    $subjectVIOR = $this->input->post('subjectVIOR');
                    $gradeVIOR = $this->input->post('gradeVIOR');
                    $gradeVIORNO = $this->input->post('gradeVIORNO');


                    if ($subject_form4) {
                        foreach ($subject_form4 as $k => $v) {
                            if ($grade_form4[$k] <> '' && $v <> '') {
                                $form4_data[$v] = $grade_form4[$k];
                            }
                        }
                    }

                    if ($subject_form6) {
                        foreach ($subject_form6 as $k => $v) {
                            if ($grade_form6[$k] <> '' && $v <> '') {
                                $form6_data[$v] = $grade_form6[$k];
                            }
                        }
                    }


                    $array_data = array(
                        'AYear' => $row_year->AYear,
                        'entry' => $this->input->post('entry'),
                        'form4_inclusive' => json_encode($form4_data),
                        'form4_exclusive' => ($this->input->post('subject4_exclusive') ? implode(',', $this->input->post('subject4_exclusive')) : ''),
                        'form4_pass' => trim($this->input->post('form4_pass')),
                        'form6_inclusive' => json_encode($form6_data),
                        'form6_exclusive' => ($this->input->post('subject6_exclusive') ? implode(',', $this->input->post('subject6_exclusive')) : ''),
                        'min_point' => ($this->input->post('min_point') ? $this->input->post('min_point') : ''),
                        'form6_pass' => trim($this->input->post('form6_pass')),
                        'gpa_pass' => trim($this->input->post('gpa_pass')),
                        'keyword1' => trim($this->input->post('keyword1')),
                        'ProgrammeCode' => $code,
                        'createdby' => $current_user->id,
                        'createdon' => date('Y-m-d H:i:s'),
                        'form4_or_subject' => ($gradeIVOR ? json_encode(array($gradeIVOR . '|' . $gradeIVORNO => $subjectIVOR)) : ''),
                        'form6_or_subject' => ($gradeVIOR ? json_encode(array($gradeVIOR . '|' . $gradeVIORNO => $subjectVIOR)) : ''),
                    );


                    $conf = $this->setting_model->programme_setting_criteria($array_data);
                    if ($conf) {
                        $this->session->set_flashdata('message', show_alert('Selection Criteria Saved successfully', 'success'));
                        redirect(current_full_url(), 'refresh');
                    } else {
                        $this->data['message'] = show_alert('Fail to save Criteria Information', 'info');
                    }
                }

                $setting_info = $this->setting_model->get_selection_criteria($code, $row_year->AYear, $ENTRY);

                if ($setting_info) {
                    $this->data['setting_info'] = $setting_info;
                }
                $this->data['content_view'] = "panel/set_criteria_rules";
                //}else{
                //  $this->data['content_view'] = "panel/set_criteria_category";
                //}
            } else {
                $this->data['message'] = show_alert('No active Year created, No Configuration allowed', 'info');
            }
            $this->data['programme_info'] = $this->db->where('Code', $code)->get('programme')->row();
            $this->data['subject_listIV'] = $this->setting_model->get_sec_subject(null, 1, 1)->result();
            $this->data['subject_listVI'] = $this->setting_model->get_sec_subject(null, 1, 2)->result();


            $this->load->view("panel/programme_setting_panel", $this->data);

        } else {
            echo "Please use link in the left side to start setting";
        }
    }

    function short_listed()
    {
        $current_user = current_user();
        if (!has_role($this->MODULE_ID, $this->GROUP_ID, 'APPLICANT', 'short_listed')) {
            $this->session->set_flashdata("message", show_alert("APPLICANT_SHORT_LISTED :: Access denied !!", 'info'));
            redirect(site_url('dashboard'), 'refresh');
        }
        $this->data['bscrum'][] = array('link' => '#', 'title' => 'Applicant');
        $this->data['bscrum'][] = array('link' => 'short_listed/', 'title' => 'Applicant Short Listed');


        $this->data['programme_list'] = $this->common_model->get_programme(null, null)->result();
        $this->data['active_menu'] = 'applicant_list';
        $this->data['content'] = 'panel/short_listed';
        $this->load->view('template', $this->data);

    }

    function run_eligibility()
    {
        $programme_list = $this->common_model->get_programme()->result();
        foreach ($programme_list as $key => $value) {
            $new = $this->db->insert('run_eligibility', array('ProgrammeCode' => $value->Code));
            $last_id = $this->db->insert_id();
            if ($last_id) {
                execInBackground('response run_eligibility ' . $last_id);
            }
        }

        $this->session->set_flashdata('message', show_alert('This process will take some time to finish. Please Wait ...', 'info'));
        redirect('short_listed', 'refresh');


    }

    function run_eligibility_active()
    {
        $check = $this->db->get("run_eligibility")->row();
        if (!$check) {
            $this->session->set_flashdata('message', show_alert('Run Eligibility completed, Please Continue with other activities ', 'success'));

        } else {
            $this->session->set_flashdata('message', show_alert('This process will take some time to finish. Please still wait ...', 'info'));
        }
        echo '1';
    }

    function collection()
    {
        $current_user = current_user();

        $this->data['bscrum'][] = array('link' => '#', 'title' => 'Fee');
        $this->data['bscrum'][] = array('link' => 'collection/', 'title' => 'Application Fee');

        $this->load->library('pagination');

        $where = ' WHERE 1=1 ';

        if (isset($_GET['from']) && $_GET['from'] != '') {
            $where .= " AND DATE(p.createdon) ='" . format_date($_GET['from']) . "' ";
        }

        if (isset($_GET['to']) && $_GET['to'] != '') {
            $where .= " AND DATE(p.createdon) ='" . format_date($_GET['to']) . "' ";
        }

        if (isset($_GET['key']) && $_GET['key'] != '') {
            $where .= " AND  (a.form4_index LIKE '%" . $_GET['key'] . "%' OR  p.msisdn LIKE '%" . $_GET['key'] . "%' 
             OR p.reference LIKE '%" . $_GET['key'] . "%' OR a.FirstName LIKE '%" . $_GET['key'] . "%' OR  a.LastName LIKE '%" . $_GET['key'] . "%')";
        }


        $sql = " SELECT p.*,a.FirstName,a.MiddleName,a.LastName FROM application_payment as p INNER JOIN application as a ON (p.applicant_id=a.id)  $where ";
        $sql2 = " SELECT count(p.id) as counter FROM application_payment as p INNER JOIN application as a ON (p.applicant_id=a.id)  $where ";

        $config["base_url"] = site_url('collection/');

        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        if (count($_GET) > 0)
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");


        $config["total_rows"] = $this->db->query($sql2)->row()->counter;
        $config["per_page"] = 50;
        $config["uri_segment"] = 2;

        include 'include/config_pagination.php';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2) ? $this->uri->segment(2) : 0);
        $this->data['pagination_links'] = $this->pagination->create_links();

        $this->data['collection_list'] = $this->db->query($sql . " ORDER BY p.createdon DESC LIMIT $page," . $config["per_page"])->result();


        $this->data['active_menu'] = 'collection';
        $this->data['content'] = 'panel/collection';
        $this->load->view('template', $this->data);
    }

    function selection_criteria($type = 1)
    {
        $current_user = current_user();

        if (!has_role($this->MODULE_ID, $this->GROUP_ID, 'CRITERIA', 'selection_criteria')) {
            $this->session->set_flashdata("message", show_alert("SELECTION_CRITERIA :: Access denied !!", 'info'));
            redirect(site_url('dashboard'), 'refresh');
        }

        $this->data['bscrum'][] = array('link' => '#', 'title' => 'Criteria');
        $this->data['bscrum'][] = array('link' => 'selection_criteria/', 'title' => 'Selection Criteria');
        $this->data['selected'] = $type;
        $this->data['programme_list'] = $this->common_model->get_programme(null, $type)->result();
        $this->data['active_menu'] = 'manage_criteria';
        $this->data['content'] = 'panel/selection_criteria';
        $this->load->view('template', $this->data);
    }

    function programme_setting_selection($code = null)
    {
        $current_user = current_user();
        $this->data['CODE'] = $code;

        $CATEGORY = (isset($_GET) && isset($_GET['category']) ? $_GET['category'] : null);
        if (!is_null($code)) {
            $row_year = $this->common_model->get_academic_year(null, 1, 1)->row();
            if ($row_year) {

                if(isset($_GET['sub_id'])) {
                    //remove one row in the setting configurations
                    $get_row = $this->db->where('id', $_GET['sub_id'])->get('application_selection_criteria_filter')->row();

                    if ($get_row) {
                        $this->db->delete('application_selection_criteria_filter', array('id' => $get_row->id));
                        $this->session->set_flashdata('message', show_alert('Setting Data updated successfully', 'info'));
                        redirect(remove_query_string(array('sub_id')), 'refresh');

                    }
                }

                if ($this->input->post('capacity')) {
                    $capacity = $this->input->post('capacity');
                    $direct = $this->input->post('direct');

                    if (is_numeric($capacity) && is_numeric($direct)) {
                        $capacity_array = array(
                            'capacity' => $capacity,
                            'code' => $code,
                            'direct' => $direct
                        );
                        $row_check = $this->db->where('code', $code)->get('application_selection_criteria')->row();
                        if ($row_check) {
                            //update
                            $this->db->update('application_selection_criteria',$capacity_array,array('code'=>$code));
                        } else {
                            //insert
                            $this->db->insert('application_selection_criteria',$capacity_array);
                        }
                    }
                }
                $row_check = $this->db->where('code', $code)->get('application_selection_criteria')->row();
                if($row_check){
                    $this->data['setting_info'] = $row_check;
                    $this->data['CATEGORY'] = $CATEGORY;

                    if($this->input->post('applicant_category')){
                        $subjectIV_submitted = $this->input->post('subjectIV[]');
                        $subjectIV_submitted_order = $this->input->post('gradeIV[]');
                        foreach ($subjectIV_submitted as $ky=>$vy){
                            if($vy <> '' && $subjectIV_submitted_order[$ky] <> '' && is_numeric($subjectIV_submitted_order[$ky])) {
                                $array_subjectIV = array(
                                    'selection_id' => $row_check->id,
                                    'code' => $code,
                                    'category' => $CATEGORY,
                                    'filter_type' => 'FORM_IV',
                                    'filter_item' => $vy,
                                );

                                $check_case = $this->db->where($array_subjectIV)->get('application_selection_criteria_filter')->row();
                                if($check_case){
                                    //update
                                    $array_subjectIV['order_number'] = $subjectIV_submitted_order[$ky];
                                    $this->db->update('application_selection_criteria_filter',$array_subjectIV,array('id'=>$check_case->id));
                                }else{
                                    //insert['
                                    $array_subjectIV['order_number'] = $subjectIV_submitted_order[$ky];
                                    $this->db->insert('application_selection_criteria_filter',$array_subjectIV);
                                }

                            }
                        }

                        $subjectVI_submitted = $this->input->post('subjectVI[]');
                        $subjectVI_submitted_order = $this->input->post('gradeVI[]');
                        foreach ($subjectVI_submitted as $ky=>$vy){
                            if($vy <> '' && $subjectVI_submitted_order[$ky] <> '' && is_numeric($subjectVI_submitted_order[$ky])) {
                                $array_subjectVI = array(
                                    'selection_id' => $row_check->id,
                                    'code' => $code,
                                    'category' => $CATEGORY,
                                    'filter_type' => 'FORM_VI',
                                    'filter_item' => $vy,
                                );

                                $check_case = $this->db->where($array_subjectVI)->get('application_selection_criteria_filter')->row();
                                if($check_case){
                                    //update
                                    $array_subjectVI['order_number'] = $subjectVI_submitted_order[$ky];
                                    $this->db->update('application_selection_criteria_filter',$array_subjectVI,array('id'=>$check_case->id));
                                }else{
                                    //insert['
                                    $array_subjectVI['order_number'] = $subjectVI_submitted_order[$ky];
                                    $this->db->insert('application_selection_criteria_filter',$array_subjectVI);
                                }

                            }
                        }

                        $point_submitted = $this->input->post('point');
                        if($point_submitted <> '') {
                            $array_point = array(
                                'selection_id' => $row_check->id,
                                'code' => $code,
                                'category' => $CATEGORY,
                                'filter_type' => 'POINT',
                                'filter_item' => 'POINT',
                            );
                            $check_case = $this->db->where($array_point)->get('application_selection_criteria_filter')->row();
                            if ($check_case) {
                                //update
                                $array_point['order_number'] = $point_submitted;
                                $this->db->update('application_selection_criteria_filter', $array_point, array('id' => $check_case->id));
                            } else {
                                //insert'
                                $array_point['order_number'] = $point_submitted;
                                $this->db->insert('application_selection_criteria_filter', $array_point);
                            }
                        }

                        $gender_submitted = $this->input->post('gender');
                        $gender_order_submitted = $this->input->post('gender_order');
                        if($gender_submitted <> '' && $gender_order_submitted <> '') {
                            $array_gender = array(
                                'selection_id' => $row_check->id,
                                'code' => $code,
                                'category' => $CATEGORY,
                                'filter_type' => 'GENDER'
                            );

                            $check_case = $this->db->where($array_gender)->get('application_selection_criteria_filter')->row();
                            if ($check_case) {
                                //update
                                $array_gender['order_number'] = $gender_order_submitted;
                                $array_gender['filter_item'] = $gender_submitted;
                                $this->db->update('application_selection_criteria_filter', $array_gender, array('id' => $check_case->id));
                            } else {
                                //insert'
                                $array_gender['order_number'] = $gender_order_submitted;
                                $array_gender['filter_item'] = $gender_submitted;
                                $this->db->insert('application_selection_criteria_filter', $array_gender);
                            }
                        }

                        $fifo_submitted = $this->input->post('fifo');
                        if($fifo_submitted <> '') {
                            $array_fifo = array(
                                'selection_id' => $row_check->id,
                                'code' => $code,
                                'category' => $CATEGORY,
                                'filter_type' => 'FIFO',
                                'filter_item' => 'FIFO',
                            );
                            $check_case = $this->db->where($array_fifo)->get('application_selection_criteria_filter')->row();

                            if($check_case) {
                                //update
                                $array_fifo['order_number'] = $fifo_submitted;
                                $this->db->update('application_selection_criteria_filter', $array_fifo, array('id' => $check_case->id));
                            }else {
                                //insert'
                                $array_fifo['order_number'] = $fifo_submitted;
                                $this->db->insert('application_selection_criteria_filter', $array_fifo);
                            }
                        }
                    }

                    $this->data['subject_listIV'] = $this->setting_model->get_sec_subject(null, 1, 1)->result();
                    $this->data['subject_listVI'] = $this->setting_model->get_sec_subject(null, 1, 2)->result();
                }

                $this->data['content_view'] = "panel/set_criteria_rules";
            } else {
                $this->data['message'] = show_alert('No active Year created, No Configuration allowed', 'info');
            }
            $this->data['programme_info'] = $this->db->where('Code', $code)->get('programme')->row();
            $this->data['subject_list'] = $this->setting_model->get_sec_subject(null, 1)->result();

            $this->load->view("panel/programme_setting_selection", $this->data);

        } else {
            echo "Please use link in the left side to start setting";
        }
    }

    function applicant_selection()
    {
        $current_user = current_user();
        if (!has_role($this->MODULE_ID, $this->GROUP_ID, 'APPLICANT', 'applicant_selection')) {
            $this->session->set_flashdata("message", show_alert("APPLICANT_SELECTION :: Access denied !!", 'info'));
            redirect(site_url('dashboard'), 'refresh');
        }
        $this->data['bscrum'][] = array('link' => '#', 'title' => 'Applicant');
        $this->data['bscrum'][] = array('link' => 'applicant_selection/', 'title' => 'Applicant Selection');


        $this->data['programme_list'] = $this->common_model->get_programme(null,null)->result();
        $this->data['active_menu'] = 'applicant_list';
        $this->data['content'] = 'panel/applicant_selection';
        $this->load->view('template', $this->data);

    }

    function run_selection($choice)
    {
        $programme_list = $this->common_model->get_programme()->result();
        foreach ($programme_list as $key => $value) {
            $new = $this->db->insert('run_selection', array('ProgrammeCode' => $value->Code,'choice'=>$choice));
            $last_id = $this->db->insert_id();
            if ($last_id) {
                execInBackground('response run_selection '.$last_id);
            }
        }

        $this->session->set_flashdata('message', show_alert('This process will take some time to finish. Please Wait ...', 'info'));
        redirect('applicant_selection', 'refresh');


    }

    function run_selection_active()
    {
        $check = $this->db->get("run_selection")->row();
        if (!$check) {
            $this->session->set_flashdata('message', show_alert('Run Selection completed, Please Continue with other activities ', 'success'));

        } else {
            $this->session->set_flashdata('message', show_alert('This process will take some time to finish. Please still wait ...', 'info'));
        }
        echo '1';
    }

    function record_bank_trans(){
    $current_user = current_user();

        $this->data['bscrum'][] = array('link' => '#', 'title' => 'Application Fee');
        $this->data['bscrum'][] = array('link' => 'record_bank_trans/', 'title' => 'Record Bank Transaction');

        $this->form_validation->set_rules('receipt', 'Receipt', 'required|is_unique[application_payment.receipt]');
        $this->form_validation->set_rules('reference', 'Reference', 'required');
        $this->form_validation->set_rules('amount', 'Amount', 'required|numeric');




        if ($this->form_validation->run() == true) {
            $reference = trim($this->input->post('reference'));
            $myreference = substr($reference,3);
            $client = $this->applicant_model->get_applicant($myreference);
            if($client){
                $payment = array(
                    'msisdn'=>'BANK',
                    'reference'=>$reference,
                    'applicant_id'=> $myreference,
                    'timestamp'=> date('Y-m-d H:i:s'),
                    'receipt'=>$this->input->post('receipt'),
                    'amount'=> trim($this->input->post('amount')),
                    'charges'=>0,
                    'channel'=>2,
                    'cretatedby'=>$current_user->id
                );

                $this->db->insert('application_payment',$payment);
                $this->session->set_flashdata('message',show_alert('Information saved successfuuly','success'));
                redirect('record_bank_trans','refresh');
            }else{
                $this->data['message'] = show_alert('Invalid Reference Number !!','warning');
            }
        }

        $this->data['active_menu'] = 'collection';
        $this->data['content'] = 'panel/record_bank_trans';
        $this->load->view('template', $this->data);
    }

}
