<?php

/**
 * Created by PhpStorm.
 * User: miltone
 * Date: 5/13/17
 * Time: 9:33 AM
 */
class Api extends  CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function receive_payment()
    {
        $data_json = file_get_contents("php://input");

        $data = json_decode($data_json, TRUE);

        if ($data['ACTION'] == 'VALIDATE') {
            $reference = $data['REFERENCE'];
            $myreference = substr($reference,3);
            $client = $this->applicant_model->get_applicant($myreference);

            $array = array(
                'MKEY' => $data['MKEY'],
                'REFERENCE' => $data['REFERENCE'],
                'ACTION' => 'VALIDATE',
                'STATUS' => 'SUCCESS'
            );
            if (!$client) {
                $array['STATUS'] = 'NOT_VALID';
            } else {
                $array['STATUS'] = 'SUCCESS';

            }
            echo json_encode($array);

        } else if ($data['ACTION'] == 'TRANS') {
            $check_receipt = $this->db->where("receipt",$data['receipt'])->get("application_payment")->row();
            $return = array(
                'status'=>'',
                'receipt'=>$data['receipt'],
                'clientID'=> $data['reference']
            );

            if(!$check_receipt) {
                $applicant_id = substr($data['reference'],3);
                $client_info=  $client_data = $this->applicant_model->get_applicant($applicant_id);
                $trans_date = date('Y-m-d',strtotime($data['timestamp']));
                $trans_timestamp = date('Y-m-d H:i:s',strtotime($data['timestamp']));
                $payment = array(
                    'msisdn'=>$data['msisdn'],
                    'reference'=>$data['reference'],
                    'applicant_id'=> $applicant_id,
                    'timestamp'=> $trans_timestamp,
                    'receipt'=>$data['receipt'],
                    'amount'=> ($data['amount']-$data['charges']),
                    'charges'=>$data['charges']
                );

                $this->db->insert('application_payment',$payment);
                $return['clientID'] = $client_data->FirstName.' '.$client_data->LastName;
                $return['status'] = 'SUCCESS';


            }else{
                //DUPLICATE
                $applicant_id = substr($check_receipt->reference,3);
                $client_data = $this->applicant_model->get_applicant($applicant_id);
                $return['clientID'] = $client_data->FirstName.' '.$client_data->LastName;
                $return['status'] = 'DUPLICATE';
            }
            echo json_encode($return);
        }
    }

}