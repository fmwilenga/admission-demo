<?php

/**
 * Created by PhpStorm.
 * User: miltone
 * Date: 5/16/17
 * Time: 5:21 AM
 */
class Report extends CI_Controller
{

    private $MODULE_ID = '';
    private $GROUP_ID = '';

    function __construct()
    {
        parent::__construct();


        $this->data['CURRENT_USER'] = current_user();

        $this->form_validation->set_error_delimiters('<div class="required">', '</div>');

        $this->data['title'] = 'Administrator';

        $tmp_group = get_user_group();
        $this->GROUP_ID = $this->data['GROUP_ID'] = $tmp_group->id;
        $this->MODULE_ID = $this->data['MODULE_ID'] = $tmp_group->module_id;
    }



    function print_application($id){
        $id = decode_id($id);
        $APPLICANT = $this->applicant_model->get_applicant($id);
        if($APPLICANT) {
            $next_kin1 = $this->applicant_model->get_nextkin_info($APPLICANT->id)->result();
            if (count($next_kin1) > 0) {
                $next_kin = $next_kin1;
            }

            $referee = $this->applicant_model->get_applicant_referee($APPLICANT->id)->result();
            if (count($referee) > 0) {
                $academic_referee = $referee;
            }

            $sponsor = $this->applicant_model->get_applicant_sponsor($APPLICANT->id)->row();
            if ($sponsor) {
                $sponsor_info = $sponsor;
            }

            $employer = $this->applicant_model->get_applicant_employer($APPLICANT->id)->row();
            if ($employer) {
                $employer_info = $employer;
            }

            $education_bg = $this->applicant_model->get_education_bg(null, $APPLICANT->id);
            $attachment_list = $this->applicant_model->get_attachment($APPLICANT->id);
            $mychoice1 = $this->applicant_model->get_programme_choice($APPLICANT->id);
            if ($mychoice1) {
                $mycoice = $mychoice1;
            }




            include_once 'report/print_application.php';
        }else{
            $this->session->set_flashdata('message',show_alert('This request did not pass our security checks.','info'));
            $current_user_group = get_user_group();
            if($current_user_group->id == 4){
                redirect(site_url('applicant_dashboard'), 'refresh');
            }else {
                redirect(site_url('dashboard'), 'refresh');
            }
        }
    }

    function applicant_byProgramme(){
        $programme = (isset($_GET) && isset($_GET['programme'])  ) ? $_GET['programme'] :null;
        $application_type = (isset($_GET) && isset($_GET['type'])  ) ? $_GET['type'] :null;
        $applicant_list =  $this->db->query("SELECT ap.*,pc.choice,pc.status as eligible,pc.comment,pc.point,pc.form6_subject,pc.form4_subject,pc.gpa,pc.diploma_info FROM application as ap  INNER JOIN application_elegibility as pc ON (ap.id=pc.applicant_id) WHERE 1=1 
                      AND pc.ProgrammeCode='$programme' ORDER BY pc.status DESC,pc.point DESC  ")->result();
        $row_year = $this->common_model->get_academic_year(null, 1, 1)->row();
        $ayear = $row_year->AYear;

        include 'report/applicant_byProgramme.php';


    }

    function export_applicant(){
        $ayear = $this->common_model->get_academic_year(null, 1, 1)->row()->AYear;
        $key =  $type = $entry =null;
        if(isset($_GET) && isset($_GET['key']) && isset($_GET['type'])){
            $key = $_GET['key'];
            $type = $_GET['type'];
            
        }
          
          if (isset($_GET['from']) && $_GET['from'] != '') {
                  $f = $_GET['from'];
                  $from = format_date($f, true);
          }

           if (isset($_GET['to']) && $_GET['to'] != '') {
                  $t = $_GET['to'];
                  $to = format_date($t, true);
              }

        if (isset($_GET['status']) && $_GET['status'] != '') {
          $status= " AND submitted='" . $_GET['status'] . "' ";
            }

        if(!is_null($type) && $type <> ''){
            //single type of application - one Worksheet
            $application_type = application_type($type);
            include_once 'report/applicant_single_sheet.php';
            exit;
        }else{
            //all application Type - 3 worksheet in excel
            //For time being force to select Application Type First
            $this->session->set_flashdata('message',show_alert('Please select type of Application before click export button','info'));
            redirect('applicant_list','refresh');
        }


    }

  function applicant_byProgramme_selected(){
        $programme = (isset($_GET) && isset($_GET['programme'])  ) ? $_GET['programme'] :null;
        $application_type = (isset($_GET) && isset($_GET['type'])  ) ? $_GET['type'] :null;
        $applicant_list =  $this->db->query("SELECT ap.*,pc.choice,pc.status as eligible,pc.selected,pc.comment,pc.point,pc.form6_subject,pc.form4_subject,pc.gpa,pc.diploma_info FROM application as ap  INNER JOIN application_elegibility as pc ON (ap.id=pc.applicant_id) WHERE 1=1 
                      AND pc.ProgrammeCode='$programme' AND pc.selected=1 ORDER BY selected_counter  ASC  ")->result();
        $row_year = $this->common_model->get_academic_year(null, 1, 1)->row();
        $ayear = $row_year->AYear;

        include 'report/applicant_byProgramme_selected.php';


    }

    function export_applicant_selected(){
        $ayear = $this->common_model->get_academic_year(null, 1, 1)->row()->AYear;
        $key =  $type = $entry =null;
        if(isset($_GET) && isset($_GET['key']) && isset($_GET['type']) && isset($_GET['entry'])){
            $key = $_GET['key'];
            $type = $_GET['type'];
            $entry = $_GET['entry'];
        }

        if(!is_null($type) && $type <> ''){
            //single type of application - one Worksheet
            $application_type = application_type($type);
            include_once 'report/applicant_single_sheet_selected.php';
            exit;
        }else{
            //all application Type - 3 worksheet in excel
            //For time being force to select Application Type First
            $this->session->set_flashdata('message',show_alert('Please select type of Application before click export button','info'));
            redirect('applicant_list','refresh');
        }


    }



      function export_collection(){
        $ayear = $this->common_model->get_academic_year(null, 1, 1)->row()->AYear;
        $key =  $from = $to =null;
        if(isset($_GET) && isset($_GET['key']) && isset($_GET['from']) && isset($_GET['to'])){
            $key = $_GET['key'];
            $to = $_GET['to'];
            $from = $_GET['from'];
        }

     include_once 'report/applicant_collection.php';
exit;

    }



}
